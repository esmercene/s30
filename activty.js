// -----Activity No. 2 - count the total number of fruits on sale------

db.fruits.aggregate(
  [
    {
      $match: {
        stock: {
          $gte: 15
        }
      }
    },
    {
      $count: "fruitsOnSale"
    }
  ]
)


// ------Activity No. 3 - count the total number of fruits with stock more than 20-------


db.fruits.aggregate(
  [
    {
      $match: {
        stock: {
          $gt: 20
        }
      }
    },
    {
      $count: "enoughStocks"
    }
  ]
)


// ------Activity No. 4 - get the average price of fruits onSale per supplier-------
db.fruits.aggregate([
	{
		$match: { onSale: true }
	},
	{
		$group: {
			_id: "$supplier_id",
			avg_price: { $avg: "$price" }
		}
	}
])

// ------Activity No. 5 - get the highest price of fruits per supplier-------

db.fruits.aggregate([
	{
		$match: { onSale: true }
	},
	{
		$group: {
			_id: "$supplier_id",
			max_price: { $max: "$price" }
		}
	}
])


// ------Activity No. 6 - get the lowest price of fruits per supplier-------


db.fruits.aggregate([
	{
		$match: { onSale: true }
	},
	{
		$group: {
			_id: "$supplier_id",
			min_price: { $min: "$price" }
		}
	}
])
